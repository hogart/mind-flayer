# Preparation

0. Launch KenkuFM and turn on the ["Remote" feature](https://www.kenku.fm/docs/using-kenku-remote).
0. Find out your IP address. (On Windows: run `ipconfig` command). Change KenkuFM settings to this IP address. It can't be `127.0.0.1`! Usually it looks like `192.168.0.10`, depending on your router.
0. In Mind Flayer settings, change KenkuFM address to the same IP.

# Adding buttons

0. In KenkuFM, click on "⋮" button on a playlist or a sound in a soundboard, and click "Copy ID". There are different ways to pass it to your mobile device; the simplest way is to send yourself an email.
0. The main screen of the Mind Flayer has an icon in a top right corner. Tap it to switch between edit and playback mode. Edit mode is indicated by an "open lock" icon.
0. In edit mode, tap "+" button. Enter the ID from step 1, the text to display on button (appropriate emoji can look great), and toggle the switch, if the button should launch the playlist.
0. Tap "Add" to save new button.
0. Once you add a few buttons, you can rearrange them: tap and hold the button, and then drag it to desired location.
1. In edit mode, tap on button to edit it's text or ID.

# Import/export

* You can copy the content of the textfield on "Import/export" screen for backup or to send it to another device.
* If you're comfortable with JSON, you can edit it manually or even create it from scratch on your computer.

