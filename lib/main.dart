import 'package:flutter/material.dart';
import 'package:mind_flayer/app_di.dart';

void main() {
  runApp(const AppDI());
}