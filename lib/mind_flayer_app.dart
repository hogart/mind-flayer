import 'package:flutter/material.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'screen/impex/impex.dart';
import 'screen/help/help.dart';
import 'screen/settings/settings.dart';
import 'screen/deck/deck.dart';

class MindFlayerApp extends StatelessWidget {
  const MindFlayerApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mind Flayer',
      theme: _flayerTheme(context),
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''),
        Locale('ru', ''),
        Locale('ua', ''),
      ],
      initialRoute: '/',
      routes: {
        '/': (context) => const Deck(),
        '/settings': (context) => const Settings(),
        '/impex': (context) => const Impex(),
        '/help': (context) => const Help(),
      },
    );
  }

  ThemeData _flayerTheme(BuildContext context) {
    return ThemeData(
      brightness: Theme.of(context).brightness,
      primarySwatch: Colors.deepPurple,
    );
  }
}
