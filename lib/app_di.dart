import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'mind_flayer_app.dart';
import 'model/deck_settings_model.dart';
import 'model/grid_settings_model.dart';
import 'model/kenku_settings_model.dart';

class AppDI extends StatelessWidget {
  const AppDI({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => GridSettingsModel()),
        ChangeNotifierProvider(create: (context) => KenkuSettingsModel()),
        ChangeNotifierProvider(create: (context) => DeckSettingsModel()),
      ],
      child: const MindFlayerApp(),
    );
  }
}