import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SharedDrawer extends StatelessWidget {
  const SharedDrawer({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final i18n = AppLocalizations.of(context)!;
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            padding: const EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
            child: const Image(image: AssetImage('assets/images/logo_white.png')),
          ),

          ListTile(
            title: Text(i18n.deckScreen),
            leading: const Icon(Icons.grid_3x3),
            onTap: () {
              Navigator.popAndPushNamed(context, '/');
            },
          ),

          ListTile(
            title: Text(i18n.settingsScreen),
            leading: const Icon(Icons.settings),
            onTap: () {
              Navigator.popAndPushNamed(context, '/settings');
            },
          ),

          ListTile(
            title: Text(i18n.impexScreen),
            leading: const Icon(Icons.import_export),
            onTap: () {
              Navigator.popAndPushNamed(context, '/impex');
            },
          ),
          
          ListTile(
            title: Text(i18n.helpScreen),
            leading: const Icon(Icons.help_outline),
            onTap: () {
              Navigator.popAndPushNamed(context, '/help');
            },
          ),
        ]
      ),
    );
  }
}