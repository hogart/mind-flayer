import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'button_dto.dart';

class DeckSettingsModel extends ChangeNotifier {
  late SharedPreferences prefs;

  String _deckJson = '[]';
  bool _isLocked = false;

  DeckSettingsModel() {
    _loadState();  
  }

  List<ButtonDto> get deck {
    try {
    final decoded = (json.decode(_deckJson) as List);
    return decoded
      .map((item) => ButtonDto.fromJson(item))
      .toList();
    } catch (e) {
      return [];
    }
  }

  ButtonDto getButtonAt(int index) {
    if (index == -1 || index > deck.length) {
      return ButtonDto.empty();
    } else {
      return deck[index];
    }
  }

  String get deckStr => _deckJson;

  bool get isLocked => _isLocked;
  
  _loadState() async {
    prefs = await SharedPreferences.getInstance();

    _deckJson = prefs.getString('deckJson') ?? '[]';
    _isLocked = prefs.getBool('isLocked') ?? false;
  }

  _saveState() async {
    await prefs.setString('deckJson', _deckJson);
    await prefs.setBool('isLocked', _isLocked);
  }

  setDeckJson(String deckJson) async {
    _deckJson = deckJson;
    await _saveState();
    notifyListeners();
  }

  addButton(ButtonDto btnDto) {
    _deckJson = json.encode(
      [...deck, btnDto]
    );
    _saveState();
    notifyListeners();
  }

  dropButton(int index) {
    final newList = List.from(deck);
    newList.removeAt(index);

    _deckJson = json.encode(newList);
    _saveState();
    notifyListeners();
  }

  updateButton(int index, ButtonDto btnDto) {
    final newList = List.from(deck);
    newList[index] = btnDto;
    _deckJson = json.encode(newList);
    _saveState();
    notifyListeners();
  }

  swapButtons(int targetIndex, int droppedIndex) {
    final newList = List.from(deck);
    final temp = newList[targetIndex];
    newList[targetIndex] = newList[droppedIndex];
    newList[droppedIndex] = temp;

    _deckJson = json.encode(newList);
    _saveState();
    notifyListeners();
  }

  validateJsonString(String deckJson) {
    try {
      json.decode(deckJson);
    } catch (e) {
      return false;
    }

    return true;
  }

  toggleLock() {
    _isLocked = !_isLocked;
    prefs.setBool('isLocked', _isLocked);
    notifyListeners();
  }
}