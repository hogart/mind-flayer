class ButtonDto {
  String text;
  String id;
  bool isPlaylist;

  ButtonDto(this.text, this.id, this.isPlaylist);

  toJson() {
    return {
      'id': id,
      'text': text,
      'isPlaylist': isPlaylist,
    };
  }

  @override
  toString() {
    return '$id:$text, $isPlaylist';
  }

  factory ButtonDto.fromJson(Map<String, dynamic> data) {
    return ButtonDto(
      data['text'] ?? '-No text-', 
      data['id'] ?? '',
      data['isPlaylist'] ?? false,
    );
  }

  factory ButtonDto.empty() {
    return ButtonDto(
      '',
      '',
      false,
    );
  }
}