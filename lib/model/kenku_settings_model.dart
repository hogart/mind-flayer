import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'button_dto.dart';

class KenkuSettingsModel extends ChangeNotifier {
  late SharedPreferences prefs;
  
  int _port = 3333;
  String _address = '';

  KenkuSettingsModel() {
    _loadState();
  }

  int get port => _port;
  String get address => _address;

  void _loadState() async {
    prefs = await SharedPreferences.getInstance();

    _port = prefs.getInt('kenkuPort') ?? 3333;
    _address = prefs.getString('kenkuAddress') ?? '';
  }

  _saveState() async {
    prefs.setInt('kenkuPort', _port);
    prefs.setString('kenkuAddress', _address);
  }

  setPort(int port) async {
    _port = port;
    await _saveState();
    notifyListeners();
  }

  setAddress(String address) async {
    _address = address;
    await _saveState();
    notifyListeners();
  }

  setData(String address, int port) async {
    _address = address;
    _port = port;
    await _saveState();
    notifyListeners();
  }

  Future<http.Response> sendRequest(ButtonDto btnDto) {
    final uri = Uri(
      scheme: 'http',
      host: _address,
      port: _port,
      path: '/v1/${btnDto.isPlaylist ? "playlist" : "soundboard"}/play',
    );

    return http.put(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: json.encode(<String, String>{
        'id': btnDto.id,
      }),
    ).timeout(
      const Duration(seconds: 1),
      onTimeout: () {
        return http.Response(
          'Check that KenkuFM is running and the address is correct.',
          408
        );
      },
    );
  }
}