import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GridSettingsModel extends ChangeNotifier {
  late SharedPreferences prefs;
  
  int _portraitCols = 3;
  int _landscapeCols = 6;

  GridSettingsModel() {
    _loadState();
  }

  int get portraitCols => _portraitCols;
  int get landscapeCols => _landscapeCols;

  _loadState() async {
    prefs = await SharedPreferences.getInstance();

    _portraitCols = prefs.getInt('portraitCols') ?? 3;
    _landscapeCols = prefs.getInt('landscapeCols') ?? 6;

    notifyListeners();
  }

  _saveState() async {
    await prefs.setInt('portraitCols', _portraitCols);
    await prefs.setInt('landscapeCols', _landscapeCols);
  }

  setPortraitCols(int cols) async {
    _portraitCols = cols;
    await _saveState();
    notifyListeners();
  } 

  setLandscapeCols(int cols) async {
    _landscapeCols = cols;
    await _saveState();
    notifyListeners();
  }

  setData(int portraitCols, int landscapeCols) async {
    _portraitCols = portraitCols;
    _landscapeCols = landscapeCols;

    await _saveState();
    notifyListeners();
  }

  void reset() {
    _portraitCols = 3;
    _landscapeCols = 6;

    _saveState();
  }
}