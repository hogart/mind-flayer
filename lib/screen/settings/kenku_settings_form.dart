import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../model/kenku_settings_model.dart';

class KenkuSettingsForm extends StatefulWidget {
  const KenkuSettingsForm({ Key? key }) : super(key: key);

  @override
  State<KenkuSettingsForm> createState() => _KenkuSettingsFormState();
}

class _KenkuSettingsFormState extends State<KenkuSettingsForm> {
  late String _address = '';
  late int _port = 3333;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Consumer<KenkuSettingsModel>(
      builder: (context, kenkuSettings, child) {
        final i18n = AppLocalizations.of(context)!;

        return Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextFormField(
                autocorrect: false,
                initialValue: kenkuSettings.address,
                decoration: InputDecoration(
                  labelText: i18n.settingsScreen_kenkuAddressLabel,
                  hintText: i18n.settingsScreen_kenkuAddressHint,
                ),
                onChanged: (String value) {
                  setState(() {
                    _address = value;
                  });
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return i18n.settingsScreen_kenkuAddressInvalid;
                  } else {
                    return null;
                  }
                },
              ),
              TextFormField(
                autocorrect: false,
                initialValue: kenkuSettings.port.toString(),
                decoration: InputDecoration(
                  labelText: i18n.settingsScreen_kenkuPortLabel,
                  hintText: i18n.settingsScreen_kenkuPortHint,
                ),
                keyboardType: const TextInputType.numberWithOptions(decimal: false, signed: false),
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                onChanged: (String value) {
                  setState(() {
                    _port = int.parse(value);
                  });
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return i18n.settingsScreen_kenkuPortInvalid;
                  } else {
                    return null;
                  }
                },
              ),
              ElevatedButton(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    await kenkuSettings.setData(_address, _port);
                  }

                  Navigator.of(context).pushNamed('/');
                }, 
                child: Text(i18n.shared_saveBtn),
              )
            ],
          ),
        );
      }
    );
  }
}
