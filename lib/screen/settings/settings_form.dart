import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../model/grid_settings_model.dart';
import 'grid_settings_form.dart';
import 'kenku_settings_form.dart';

class SettingsForm extends StatelessWidget {
  const SettingsForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<GridSettingsModel>(
      builder: (context, gridSettings, child) {
        return ListView(
          children: [
            const GridSettingsForm(),

            Divider(
              color: Theme.of(context).primaryColorDark,
            ),

            const KenkuSettingsForm(),
          ],
        );
      }
    );
  }
}