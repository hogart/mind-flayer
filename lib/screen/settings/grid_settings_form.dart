import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../model/grid_settings_model.dart';

class GridSettingsForm extends StatefulWidget {
  const GridSettingsForm({Key? key}) : super(key: key);

  @override
  State<GridSettingsForm> createState() => _GridSettingsFormState();
}

class _GridSettingsFormState extends State<GridSettingsForm> {
  late GridSettingsModel gridSettings;
  late int _portraitCols;
  late int _landscapeCols;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    gridSettings = Provider.of<GridSettingsModel>(context, listen: false);
    _portraitCols = gridSettings.portraitCols;
    _landscapeCols = gridSettings.landscapeCols;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final i18n = AppLocalizations.of(context)!;
    var textTheme = Theme.of(context).textTheme;

    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            i18n.settingsScreen_portraitCols(_portraitCols),
            style: textTheme.labelMedium,
          ),
          Slider(
            value: _portraitCols.toDouble(),
            min: 2,
            max: 12,
            divisions: 10,
            label: _portraitCols.toString(),
            onChanged: (double value) {
              setState(() {
                _portraitCols = value.toInt();
              });
            },
          ),
          Text(
            i18n.settingsScreen_landscapeCols(_landscapeCols),
            style: textTheme.labelMedium,
          ),
          Slider(
            value: _landscapeCols.toDouble(),
            min: 2,
            max: 24,
            divisions: 22,
            label: _landscapeCols.toString(),
            onChanged: (double value) {
              setState(() {
                _landscapeCols = value.toInt();
              });
            },
          ),
          ElevatedButton(
            onPressed: () async {
              await gridSettings.setData(_portraitCols, _landscapeCols);

              Navigator.of(context).pushNamed('/');
            },
            child: Text(i18n.shared_saveBtn),
          )
        ],
      ),
    );
  }
}
