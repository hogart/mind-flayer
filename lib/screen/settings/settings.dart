import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../shared_drawer.dart';
import './settings_form.dart';

class Settings extends StatelessWidget {
  const Settings({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.settingsScreen),
      ),
      drawer: const SharedDrawer(),
      body: const Padding(
        child: SettingsForm(),
        padding: EdgeInsets.all(8.0),
      ),
    );
  }
}