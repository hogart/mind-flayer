import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../model/deck_settings_model.dart';

class DeckImpexForm extends StatefulWidget {
  const DeckImpexForm({Key? key}) : super(key: key);

  @override
  State<DeckImpexForm> createState() => _DeckImpexFormState();
}

class _DeckImpexFormState extends State<DeckImpexForm> {
  final _formKey = GlobalKey<FormState>();
  late DeckSettingsModel deckSettings;
  String _deckJson = '[]';

  @override
  void initState() {
    deckSettings = Provider.of<DeckSettingsModel>(context, listen: false);
    _deckJson = deckSettings.deckStr;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<DeckSettingsModel>(
      builder: (context, deckSettings, child) {
        final i18n = AppLocalizations.of(context)!;

        return Form(
          key: _formKey,
          child: ListView(
            padding: const EdgeInsets.all(8.0),
            children: [
              TextFormField(
                maxLines: 20,
                initialValue: _deckJson,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText:
                      '[{"text": "To Arms!", "id": "7915eea3-3d81-4ead-8fb4-6164e1f0f3d3", "playlist": false}]',
                  labelText: i18n.impexScreen_jsonLabel,
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return i18n.impexScreen_empty;
                  }
                  if (!deckSettings.validateJsonString(value)) {
                    return i18n.impexScreen_jsonInvalid;
                  }
                  return null;
                },
                onChanged: (String value) {
                  if (deckSettings.validateJsonString(value)) {
                    setState(() {
                      _deckJson = value;
                    });
                  }
                },
              ),

              ElevatedButton(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    await deckSettings.setDeckJson(_deckJson);
                    Navigator.of(context).pushNamed('/');
                  }
                },
                child: Text(i18n.shared_saveBtn),
              ),
            ],
          ),
        );
      },
    );
  }
}
