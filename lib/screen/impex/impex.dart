import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../shared_drawer.dart';
import 'impex_form.dart';

class Impex extends StatelessWidget {
  const Impex({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.impexScreen),
      ),
      drawer: const SharedDrawer(),
      body: const ImpexForm(),
    );
  }
}