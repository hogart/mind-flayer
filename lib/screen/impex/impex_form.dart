import 'package:flutter/material.dart';
import 'deck_impex_form.dart';

class ImpexForm extends StatelessWidget {
  const ImpexForm({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const DeckImpexForm();
  }
}