import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../model/deck_settings_model.dart';
import '../../model/grid_settings_model.dart';
import 'btn_editor.dart';
import 'deck_btn.dart';

class DeckGrid extends StatelessWidget {
  const DeckGrid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer2<GridSettingsModel, DeckSettingsModel>(
      builder: (context, gridSettings, deckSettings, child) {
        return OrientationBuilder(builder: (context, orientation) {
          final colsCount = orientation == Orientation.portrait ? gridSettings.portraitCols : gridSettings.landscapeCols;
          final deckButtons = deckSettings.deck.mapIndexed(
            (index, btnDto) => DeckBtn(btnDto: btnDto, index: index)
          ).toList();

          final addButton = [];
          if (!deckSettings.isLocked) {
            addButton.add(_addButton(context));
          }

          return GridView.count(
            crossAxisCount: colsCount,
            children: [
              ...deckButtons,
              ...addButton,
            ],
          );
        });
      },
    );
  }

  IconButton _addButton(BuildContext context) {
    return IconButton(
      onPressed: () {
        Navigator.of(context).restorablePush(_dialogBuilder);
      }, 
      icon: Icon(
        Icons.add,
        semanticLabel: 'Add new button',
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  static Route<Object?> _dialogBuilder(BuildContext context, Object? arguments) {
    return DialogRoute<void>(
      context: context,
      builder: (BuildContext context) {
        return const BtnEditor(
          index: -1,
        );
      },
    );
  }
}