import 'package:flutter/material.dart';

import '../../shared_drawer.dart';
import 'deck_lock.dart';
import 'deck_grid.dart';

class Deck extends StatelessWidget {
  const Deck({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('MindFlayer'),
        actions: const [
          DeckLock(),
        ],
      ),
      drawer: const SharedDrawer(),
      body: const DeckGrid(),
    );
  }
}