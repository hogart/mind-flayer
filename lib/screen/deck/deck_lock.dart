import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../model/deck_settings_model.dart';

class DeckLock extends StatelessWidget {
  const DeckLock({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<DeckSettingsModel>(
      builder: (context, deckSettings, child) {
        return IconButton(
          onPressed: () {
            deckSettings.toggleLock();
          }, 
          icon: Icon(
            deckSettings.isLocked ? Icons.lock : Icons.lock_open,
            semanticLabel: deckSettings.isLocked ? 'Unlock button editing' : 'Lock button editing',
          ),
        );
      }
    );
  }
}