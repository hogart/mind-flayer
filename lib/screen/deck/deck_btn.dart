import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

import '../../model/deck_settings_model.dart';
import '../../model/kenku_settings_model.dart';
import '../../model/button_dto.dart';
import 'deck_btn_content.dart';
import 'btn_editor.dart';

class DeckBtn extends StatelessWidget {
  final ButtonDto btnDto;
  final int index;

  const DeckBtn ({ Key? key, required this.btnDto, required this.index }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<DeckSettingsModel>(
      builder: (context, deckSettings, child) {
        if (deckSettings.isLocked) {
          return _renderButton(context, deckSettings.isLocked);
        } else {
          return DragTarget<int>(
            onAccept: (draggedIndex) {
              deckSettings.swapButtons(index, draggedIndex);
            },
            builder: (context, candidateData, rejectedData) {
              return LongPressDraggable<int>(
                data: index,
                child: _renderButton(context, deckSettings.isLocked),
                feedback: DeckButtonContent(btnDto: btnDto),
              );
            },
          );
        }
      },
    );
  }

  _renderButton(BuildContext context, bool isLocked) {
    return TextButton(
      child: DeckButtonContent(btnDto: btnDto),
      onPressed: () async {
        if (isLocked) {
          await _activate(context);
        } else {
          _edit(context);
        }
      },
    );
  }

  _activate(BuildContext context) async {
    final response = await Provider.of<KenkuSettingsModel>(context, listen: false).sendRequest(btnDto);
    if (response.statusCode >= 400) {
      _showErrorSnackbar(context, response);
    }
  }

  _edit(BuildContext context) {
    final params = {'index': index};
    Navigator.of(context).restorablePush(_dialogBuilder, arguments: params);
  }

  _showErrorSnackbar(BuildContext context, Response response) {
    final message = RichText(
      text: TextSpan(
        style: TextStyle(color: Theme.of(context).errorColor),
        children: [
          const TextSpan(text: 'Error occurred during contacting KenkuFM\n'),
          TextSpan(
            text: response.body,
            style: const TextStyle(fontWeight: FontWeight.bold), 
          ),
        ] 
      )
    
    );

    final snackbar = SnackBar(
      content: message,
    );
    
    ScaffoldMessenger.of(context).showSnackBar(snackbar);
  }

  static Route<Object?> _dialogBuilder(BuildContext context, Object? params) {
    return DialogRoute<Map>(
      context: context,
      builder: (BuildContext context) {
        final index = (params as Map)['index'];

        return BtnEditor(
          index: index,
        );
      },
    );
  }
}