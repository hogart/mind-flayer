import 'package:flutter/material.dart';
import 'package:mind_flayer/model/button_dto.dart';

class DeckButtonContent extends StatelessWidget {
  final ButtonDto btnDto;

  const DeckButtonContent({ Key? key, required this.btnDto }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final textStyle = btnDto.text.runes.length <= 2
        ? textTheme.headline3
        : textTheme.bodyText1;

    return Text(
      btnDto.text,
      style: textStyle,
      textAlign: TextAlign.center,
    );
  }
}