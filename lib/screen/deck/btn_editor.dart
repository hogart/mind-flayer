import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../model/deck_settings_model.dart';
import '../../model/button_dto.dart';

class BtnEditor extends StatefulWidget {
  final int index;

  const BtnEditor({Key? key, required this.index}) : super(key: key);

  @override
  State<BtnEditor> createState() => _BtnEditorState();
}

class _BtnEditorState extends State<BtnEditor> {
  late DeckSettingsModel deckSettings;
  late ButtonDto btnDto;

  @override
  void initState() {
    deckSettings = Provider.of<DeckSettingsModel>(context, listen: false);
    btnDto = deckSettings.getButtonAt(widget.index);
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final isNewBtn = widget.index == -1;
    final i18n = AppLocalizations.of(context)!;

    return AlertDialog(
      title: Text(isNewBtn ? i18n.deckScreen_addBtn : i18n.deckScreen_editBtn),

      content: Column(
        children: [
          TextFormField(
            autocorrect: false,
            initialValue: btnDto.id,
            decoration: InputDecoration(
              labelText: i18n.deckScreen_idLabel,
              hintText: i18n.deckScreen_idHint,
            ),
            onChanged: (String id) {
              setState(() {
                btnDto.id = id;
              });
            },
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return i18n.deckScreen_idInvalid;
              } else {
                return null;
              }
            },
          ),
          TextFormField(
            autocorrect: false,
            initialValue: btnDto.text,
            decoration: InputDecoration(
              labelText: i18n.deckScreen_textLabel,
              hintText: i18n.deckScreen_textHint,
            ),
            onChanged: (String id) {
              setState(() {
                btnDto.text = id;
              });              
            },
          ),
          SwitchListTile(
            title: Text(i18n.deckScreen_isPlaylist),
            value: btnDto.isPlaylist,
            selected: btnDto.isPlaylist,

            onChanged: _onPlaylistToggle,
          ),
        ],
      ),

      actions: [
        TextButton(
          onPressed: () {
            if (!isNewBtn) {
              deckSettings.dropButton(widget.index);
            }

            Navigator.of(context).pop();
          },
          child: Text(
            isNewBtn ? i18n.deckScreen_cancel : i18n.deckScreen_delete,
            style: TextStyle(color: Theme.of(context).errorColor),
          )
        ),
        TextButton(
          onPressed: () {
            if (widget.index == -1) {
              deckSettings.addButton(btnDto);
            } else {
              deckSettings.updateButton(widget.index, btnDto);
            }

            Navigator.of(context).pop();
          },
          child: Text(
            isNewBtn ? i18n.deckScreen_add : i18n.shared_saveBtn
          ),
        ),
      ],
    );
  }

  _onPlaylistToggle(bool isPlaylist) {
    setState(() {
      btnDto.isPlaylist = isPlaylist;
    });
  }
}
