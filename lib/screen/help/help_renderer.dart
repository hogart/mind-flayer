import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_markdown/flutter_markdown.dart';

class HelpRenderer extends StatelessWidget {
  const HelpRenderer({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Locale locale = Localizations.localeOf(context);
    String language = 'en';
    if (locale.languageCode == 'ru') {
      language = 'ru';
    }
    if (locale.languageCode == 'uk') {
      language = 'uk';
    }

    return FutureBuilder<String>(
      future: _loadHelp(language),
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
        }
        if (snapshot.hasData) {
          return Markdown(data: snapshot.data!);
        } else {
          return const Center(
            child: CircularProgressIndicator()
          );
        }
      }
    );
  }

  Future<String> _loadHelp(String language) async {
    return await rootBundle.loadString('assets/text/help_$language.md');
  }
}