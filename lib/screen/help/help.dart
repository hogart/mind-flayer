import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../shared_drawer.dart';
import 'help_renderer.dart';

class Help extends StatelessWidget {
  const Help({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final i18n = AppLocalizations.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(i18n.helpScreen),
      ),
      drawer: const SharedDrawer(),
      body: const Padding(
        padding: EdgeInsets.all(8.0),
        child: HelpRenderer(),
      ),
    );
  }
}
